prefix =   $(DESTDIR)/usr
bindir =   $(prefix)/bin
man1dir =  $(prefix)/share/man/man1
libdir =   $(prefix)/lib/strap
sharedir = $(prefix)/share
all: nativeTools_unix2

nativeTools_unix2: src/nativeTools_unix2.c
	cc -lm -lX11 -o nativeTools_unix2 src/nativeTools_unix2.c

clean:
	rm -f *.jar nativeTools_unix2

install:
	mkdir -p  $(libdir) $(man1dir) $(bindir) $(sharedir)/pixmaps $(sharedir)/mime/packages $(sharedir)/applications
	install nativeTools_unix2 $(libdir)
	install -m 0644 strap.desktop $(sharedir)/applications
	install -m 0644 man/*.1 $(man1dir)
	install sh/* $(bindir)
	install  -m 0644 pixmaps/* $(sharedir)/pixmaps
	install  -m 0644 mime/* $(sharedir)/mime/packages

# (highlight-regexp "\t" 'hi-yellow)

 