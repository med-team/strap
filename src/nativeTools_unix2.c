#include <X11/Xlib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define _NET_WM_STATE_REMOVE        0    /* remove/unset property */
#define _NET_WM_STATE_ADD           1    /* add/set property */

static Window rootwin;
static Display* display;
static	XWindowChanges wc; 


/* Recursively search the window heirarchy for a window of the given name */
Window find_window(Display *display, Window top, char *name) {
  Window root, parent, *children, found;
  char *window_name;
  unsigned int N, i;
		
  /* Check if we've already got it */
  XFetchName(display, top, &window_name);
		//printf(" window_name=%s\n",window_name);
  if (!window_name==None && strcmp(window_name, name) == 0) {
    XFree(window_name);
    return top;
  }
		
  /* If not, recursively check all children */
  if (!XQueryTree(display, top, &root, &parent, &children, &N)) return 0;
  found = 0;
  for (i=0; i < N; i++) {
				//printf(" N=%d\n",N);
    found = find_window(display, children[i], name);
    if (found) break;
  }
  XFree(children);
  return found;
}

void above(Display* disp,Window win, char state) {
		long  prop2 = XInternAtom(disp, "_NET_WM_STATE_ABOVE", False);
		char *msg="_NET_WM_STATE";
		//printf("msg=%ld\n",XInternAtom(disp, msg, False));
  XEvent event;
		long mask = SubstructureRedirectMask | SubstructureNotifyMask;
		event.xclient.type = ClientMessage;
		event.xclient.serial = 0;
		event.xclient.send_event = True;
		event.xclient.message_type = XInternAtom(disp, msg, False);
		event.xclient.window = win;
		event.xclient.format = 32;
		event.xclient.data.l[0] = (state=='T' ?  _NET_WM_STATE_ADD : _NET_WM_STATE_REMOVE);
		event.xclient.data.l[1] = prop2;
		event.xclient.data.l[2] = 0;
		event.xclient.data.l[3] = 0;
		event.xclient.data.l[4] = 0;
		int success=XSendEvent(disp, DefaultRootWindow(disp), False, mask, &event);
		//	printf(" success=%d",success);
}



void set(char state, int x, int y, char title[]) {
		Window w=find_window(display, rootwin, title) ;
		if (w==0)  { printf("not found: %s\n",title); }
		else {
				if (state=='T' || state=='t')	above(display,w,state);				
				if (state=='F' ) {
						//			XUnmapWindow(display,w);
						XMapRaised(display, w);	
						wc.stack_mode=Above;	XConfigureWindow(display,w,CWStackMode,&wc);
						if (x>=0) {
								wc.x=x;	XConfigureWindow(display,w,CWX,&wc); 
								wc.y=y;	XConfigureWindow(display,w,CWY,&wc); 
						}
				}
				if (state=='f' || state=='t') XLowerWindow(display,w);				
				if (state=='I') XIconifyWindow(display,w,DefaultScreen(display));
		}
	
}

int main(int argc, char* argv[]) {

		int i;
		display = XOpenDisplay(NULL);
		
		if (display == NULL) {
				printf("Verbindung zum X-Server fehlgeschlagen\n");
				return 1;
		}
		if (argc<5) {
				printf("Wrong number of parameters\n");
				return 1;
		}
		const char state=*argv[1];
		int x=atoi(argv[2]);
		int y=atoi(argv[3]);
		const int screen = XDefaultScreen (display);		
		rootwin = RootWindow(display, screen);
		for(i=4; i<argc; i++) {
				set(state,x,y,argv[i]);			
		}
		XFlush(display);
				return 0;
}


/*
		put ~/java/charite/christo/files/sws_linux386.c -o /var/www/bioinf/strap/t/sws_linux386.c
		put ~/java/charite/christo/files/sws_windows.c -o /var/www/bioinf/strap/t/sws_windows.c

*/
// http://users.actcom.co.il/~choo/lupg/tutorials/xlib-programming/window-operations.c
//http://tucupi.cptec.inpe.br/sx4/sx4man2/g1ae02e/chap3.html

//http://www.csl.mtu.edu/cs4760/www/Lectures/HCIExamplesLectures/XWin/xWM.htm
